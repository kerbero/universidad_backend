package com.es.practicas.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.practicas.controller.dto.ClaseDTO;
import com.es.practicas.mapper.IMapper;
import com.es.practicas.services.AsignaturaService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/clases")
@CrossOrigin(origins = "*")
public class AsignaturaController {
	
	
	private AsignaturaService service;
	private IMapper mapper;
	
	public AsignaturaController(AsignaturaService service, IMapper mapper) {
		this.service = service;
		this.mapper = mapper;
	}

	@GetMapping()
	public ResponseEntity<?> findAll(){
		Map<String, Object> response = new HashMap<>();
		//response.put("Get Result Score: ", mapper.mapModelToDto(service.findAll()));
		return new ResponseEntity<>(mapper.mapModelToDto(service.findAll()), HttpStatus.OK);
	}

	
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody ClaseDTO clase, BindingResult result) throws Exception {
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> error  = result.getFieldErrors().stream().map(err -> {
				return "El campo '"+ err.getField() +"' el mensaje "+ err.getDefaultMessage();
			}).collect(Collectors.toList());
			response.put("errores", error);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		//response.put("response: ", );
		return new ResponseEntity<>(service.create(mapper.dtoToModel(clase)), HttpStatus.CREATED);
	}
}
