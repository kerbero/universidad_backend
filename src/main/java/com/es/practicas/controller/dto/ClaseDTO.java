package com.es.practicas.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClaseDTO {

	private Long id;
	private String nombre;
	private Integer creditos;
	private String profesor;
	
}
