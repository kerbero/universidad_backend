package com.es.practicas.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.es.practicas.controller.dto.ClaseDTO;
import com.es.practicas.repository.entity.ClaseEntity;
import com.es.practicas.services.model.Clase;


@Mapper(componentModel = "spring")
public interface IMapper {

	List<ClaseDTO> mapModelToDto(List<Clase> clases);
	List<Clase> mapEnityToModel(List<ClaseEntity> clases);
	ClaseEntity modelToEntity(Clase clase);
	Clase dtoToModel(ClaseDTO clase);
	Clase modelToDto(ClaseEntity save);

}
