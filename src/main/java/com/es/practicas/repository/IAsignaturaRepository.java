package com.es.practicas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.es.practicas.repository.entity.ClaseEntity;

@Repository
public interface IAsignaturaRepository extends CrudRepository<ClaseEntity, Long>{

}
