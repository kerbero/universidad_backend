package com.es.practicas.services;

import java.util.List;

import com.es.practicas.services.model.Clase;

public interface AsignaturaService {

	List<Clase> findAll();

	Clase create( Clase clase);

	
}
