package com.es.practicas.services.impl;

import java.util.List;

import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import com.es.practicas.controller.dto.ClaseDTO;
import com.es.practicas.mapper.IMapper;
import com.es.practicas.repository.IAsignaturaRepository;
import com.es.practicas.services.AsignaturaService;
import com.es.practicas.services.model.Clase;

import jakarta.validation.Valid;

@Service
public class AsignaturaServiceImpl implements AsignaturaService {

	
	private IAsignaturaRepository repository;
	private IMapper mapper;
	
	
	public AsignaturaServiceImpl(IAsignaturaRepository repository, IMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}


	@Override
	public List<Clase> findAll() {
		var listaDto = repository.findAll();
		var lista = Streamable.of(repository.findAll()).toList();
		return mapper.mapEnityToModel(Streamable.of(repository.findAll()).toList());
	}


	@Override
	public Clase create( Clase clase) {
		return mapper.modelToDto(repository.save(mapper.modelToEntity(clase)));
	}

}
