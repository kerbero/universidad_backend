package com.es.practicas.services.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Clase {

	private Long id;
	private String nombre;
	private Integer creditos;
	private String profesor;
	
}
